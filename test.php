
<?php
    ob_start();
    session_start();
    require_once('config.php');
    include ROOT."/inc/functions.php";
    spl_autoload_register("loadClass");
    $db= new Db();
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
    <?php require_once ROOT.'/templates/header.php'; ?>
	</head>
	<body>
	<div id="fh5co-page">
    <?php require_once ROOT.'/templates/leftmenu.php'; ?>
    <?php require_once ROOT.'/templates/upload.php'; ?>
	</div>
	</div>
    <?php require_once ROOT.'/templates/footer.php'; ?>
	</body>
</html>







