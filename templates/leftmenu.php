<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
		<aside id="fh5co-aside" role="complementary" class="border js-fullheight">

			<h1 id="fh5co-logo"><a href="index.php"><img src="images/logo.png" alt="Nguyễn Minh Nhựt | KANGCODE - Develop witth passion"></a></h1>
			<div class="username">
			<?php if(isset($_SESSION['user'])){
				echo "Xin chào, ".$_SESSION['user']." <a href='index.php?page=logout'>[X]</a>";
			}
			?>
			</div>
			<nav id="fh5co-main-menu" role="navigation">
				<ul>
					<li <?php if(!isset($_GET['page'])){ echo 'class="fh5co-active"';} ?> ><a href="index.php">TRANG CHỦ</a></li>
					<li <?php if(isset($_GET['page'])){ if($_GET['page'] == "topic"){ echo 'class="fh5co-active"'; }} ?>><a href="index.php?page=topic">BÀI VIẾT</a></li>
					<li <?php if(isset($_GET['page'])){ if($_GET['page'] == "about"){ echo 'class="fh5co-active"'; }} ?>><a href="index.php?page=about">TÁC GIẢ</a></li>
					<li <?php if(isset($_GET['page'])){ if($_GET['page'] == "contact"){ echo 'class="fh5co-active"'; }} ?>><a href="index.php?page=contact">LIÊN HỆ</a></li>
				</ul>
			</nav>

			<div class="fh5co-footer">
				<p><small>&copy; 2018 Nhựt Nguyễn Minh Blog. <br>All Rights Reserved.</span> <span>Designed by <a href="#" target="_blank">KangCode a.k.a Nhựt Nguyễn Minh</a> </span></small></p>
				<ul>
					<li><a href="#"><i class="icon-facebook"></i></a></li>
					<li><a href="#"><i class="icon-twitter"></i></a></li>
					<li><a href="#"><i class="icon-instagram"></i></a></li>
					<li><a href="#"><i class="icon-linkedin"></i></a></li>
				</ul>
			</div>

		</aside>