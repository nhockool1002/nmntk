<div id="fh5co-main">
			<div class="fh5co-more-contact">
				<div class="fh5co-narrow-content">
					<div class="row">
						<div class="col-md-4">
							<div class="fh5co-feature fh5co-feature-sm animate-box" data-animate-effect="fadeInLeft">
								<div class="fh5co-icon">
									<i class="icon-envelope-o"></i>
								</div>
								<div class="fh5co-text">
									<p><a href="#">nhut.nguyenminh.it@gmail.com</a></p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="fh5co-feature fh5co-feature-sm animate-box" data-animate-effect="fadeInLeft">
								<div class="fh5co-icon">
									<i class="icon-map-o"></i>
								</div>
								<div class="fh5co-text">
									<p>129/17 Mễ Cốc, Phường 15, Quận 8</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="fh5co-feature fh5co-feature-sm animate-box" data-animate-effect="fadeInLeft">
								<div class="fh5co-icon">
									<i class="icon-phone"></i>
								</div>
								<div class="fh5co-text">
									<p><a href="tel://">+84 122 893 8041</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="fh5co-narrow-content animate-box" data-animate-effect="fadeInLeft">
				
				<div class="row">
					<div class="col-sm-12">
						<?php
							if(isset($_SESSION['flash'])){
								echo $_SESSION['flash'];
								unset($_SESSION['flash']);
							}
						?>
					</div>
					<div class="col-md-12">
						<p class="thongtincontact" style="color:red;">(*) Xin chào, thông qua form này tôi sẽ biết bạn cần gì ? Tôi sẽ nhanh chóng liên hệ lại với bạn ngay thôi!</p>
					</div>
					<div class="col-md-4">
						<h1>Liên hệ với tôi</h1>
					</div>
				</div>
				<form action="exe/contactform.php" method="POST">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="form-control" name="name" placeholder="Name">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" name="email" placeholder="Email">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" name="phone" placeholder="Phone">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<textarea name="message" id="message" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
									</div>
									<div class="form-group">
										<input type="submit" name="submit" class="btn btn-primary btn-md" value="Gửi Liên hệ">
									</div>
								</div>
								
							</div>
						</div>
						
					</div>
				</form>
			</div>

		</div>