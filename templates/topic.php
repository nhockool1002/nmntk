<div id="fh5co-main">
<div class="fh5co-narrow-content">
    <div class="row animate-box" data-animate-effect="fadeInRight">
    <?php 
        $sql = "SELECT * FROM topics";
        $obj = new Db();
        $rows = $obj->select($sql);
        foreach($rows as $row){
    ?>
        <div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 work-item">
            <a href="index.php?page=viewtopic&id=<?php echo $row['id']; ?>">
                <img src="images/<?php echo $row['thumb']; ?>" alt="<?php echo $row['name']; ?>" class="img-responsive">
                <h3 class="fh5co-work-title"><?php echo $row['name']; ?></h3>
            </a>
        </div>
        <?php } ?>	
    </div>
</div>
<div class="addplus" style="width:100%;text-align:center;"><a href="index.php?page=upload-topic"><img src="images/plus.png" style="width:50px;"></a></div>
</div>