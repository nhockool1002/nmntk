<!-- jQuery -->
<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- Stellar -->
	<script src="js/jquery.stellar.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Counters -->
	<script src="js/jquery.countTo.js"></script>
    <!-- Dropzone -->
    <script src="js/dropzone.js"></script>
    <script>
    $(document).ready(function(){
    $('.add_more').click(function(e){
        e.preventDefault();
        $(".firstinput").after("<input name='file[]' type='file' class='form-control' placeholder='' aria-describedby='helpId' />");
    });
    });
    </script>
	<!-- lightSlider --> 
	<script src="js/lightslider.min.js"></script>
    <script>
    	 $(document).ready(function() {
            $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:9,
                slideMargin: 0,
                speed:500,
                auto:true,
                loop:true,
                keyPress: false,
                controls: true,
                prevHtml: '',
                nextHtml: '',
                onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }  
            });
		});
    </script>
	<!-- MAIN JS -->
	<script src="js/main.js"></script>