<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Nguyễn Minh Nhựt | KANGCODE - Develop witth passion</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="KANGCODE - Blog cá nhân dùng đễ chia sẽ nhưng nội dung hữu ích cho tất cả mọi người" />
	<meta name="keywords" content="Kangcode, kangdata, nhockool1002, tài liệu, mã nguồn, sourcecode, dữ liệu" />
	<meta name="author" content="KangCode" />
	<meta property="og:title" content="Nguyễn Minh Nhựt | KANGCODE - Develop witth passion"/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"/>
	<meta property="og:site_name" content="Nguyễn Minh Nhựt | KANGCODE - Develop witth passion"/>
	<meta property="og:description" content="KANGCODE - Blog cá nhân dùng đễ chia sẽ nhưng nội dung hữu ích cho tất cả mọi người"/>
	<meta name="twitter:title" content="Nguyễn Minh Nhựt | KANGCODE - Develop witth passion" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="http://nguyenminhnhut.tk" />
	<meta name="twitter:card" content="Nguyễn Minh Nhựt" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="images/nlogo.ico">

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
    <!-- lightSlider -->
    <link rel="stylesheet" href="css/lightslider.min.css">
	<!-- Dropzone -->
	<link rel="stylesheet" href="css/dropzone.css">
	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
	    <!-- TINYMCE JS -->
		<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=5g5faf78gvk6yfq9bd3bbfjo858kjx1q8o0nbiwtygo2e4er"></script>
  <script type="text/javascript">
  tinymce.init({
    selector: 'textarea#tinymce',
	plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount   imagetools contextmenu colorpicker textpattern help',
  	toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
	textcolor_map: [
    "000000", "Black",
    "993300", "Burnt orange",
    "333300", "Dark olive",
    "003300", "Dark green",
    "003366", "Dark azure",
    "000080", "Navy Blue",
    "333399", "Indigo",
    "333333", "Very dark gray",
    "800000", "Maroon",
    "FF6600", "Orange",
    "808000", "Olive",
    "008000", "Green",
    "008080", "Teal",
    "0000FF", "Blue",
    "666699", "Grayish blue",
    "808080", "Gray",
    "FF0000", "Red",
    "FF9900", "Amber",
    "99CC00", "Yellow green",
    "339966", "Sea green",
    "33CCCC", "Turquoise",
    "3366FF", "Royal blue",
    "800080", "Purple",
    "999999", "Medium gray",
    "FF00FF", "Magenta",
    "FFCC00", "Gold",
    "FFFF00", "Yellow",
    "00FF00", "Lime",
    "00FFFF", "Aqua",
    "00CCFF", "Sky blue",
    "993366", "Red violet",
    "FFFFFF", "White",
    "FF99CC", "Pink",
    "FFCC99", "Peach",
    "FFFF99", "Light yellow",
    "CCFFCC", "Pale green",
    "CCFFFF", "Pale cyan",
    "99CCFF", "Light sky blue",
    "CC99FF", "Plum"
  ]
  });
  </script>