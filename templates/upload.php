<div id="fh5co-main">
    <div class="fh5co-narrow-content">
			<div class="row">
<?php
if(isset($_SESSION['user'])){
  if(isset($_SESSION['flash'])){
      echo $_SESSION['flash'];
      unset($_SESSION['flash']);
  }
  
?>

    <form action="exe/upload.php" enctype="multipart/form-data" method="POST" onsubmit="ProcessOn()">
<div class="form-group">
  <label for="">Tên sản phẩm : </label>
  <input type="text" name="tensp" id="tensp" class="form-control" placeholder="Nhập tên sản phẩm" aria-describedby="helpId">
</div>
<div class="form-group">
    <label for="">Nhập mô tả sản phẩm :</label>
    <textarea class="form-control" name="mota" id="tinymce" style="height:250px;"></textarea>
</div>
<div class="form-group">
  <label for="">Hình đại diện sản phẩm</label>
  <input type="file" name="thumb" id="thumb" class="form-control" placeholder="" aria-describedby="helpId">
</div>
<div class="form-group">
  <label for="">Hình sản phẩm</label>
  <input name="file[]" type="file" class="form-control firstinput" placeholder="" aria-describedby="helpId" />
</div> 
<button class="add_more btn btn-success">Thêm hình ảnh khác</button>
<br>
<div class="form-group">
  <label for="">Secret Key : </label>
  <input type="password" name="key" id="key" class="form-control" placeholder="Nhập mã bảo mật" aria-describedby="helpId">
</div>
<div class="form-group">
  <label for="">Demo : </label>
  <input type="demo" name="demo" id="demo" class="form-control" placeholder="Link Demo" aria-describedby="helpId">
</div>
   <div>
       <button type="submit" id="submitForm" name="submit" class="btn btn-danger btn-block"> GỬI </button>
   </div>
</form>
<?php 
} else{
  $_SESSION['flash'] = "<div class='alert alert-info text-center'>Vui lòng đăng nhập để sửa sản phẩm</div>"; 
  header("Location:index.php?page=login");  
}
?>
			</div>
		</div>
	</div>

