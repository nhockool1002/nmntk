<?php
if(isset($_GET['id'])){
    $id = $_GET['id'];
    $sql = "SELECT * FROM products WHERE id = '$id'";
    $obj = new Db();
    $rows = $obj->select($sql);
    foreach($rows as $row){


?>
<div id="fh5co-main">

			<div class="fh5co-narrow-content">
				<div class="row">

					<div class="col-md-12 animate-box" data-animate-effect="fadeInLeft">
                        <h1><?php echo $row['name']; ?></h1>
						<figure class="text-center">
							<img src="images/<?php echo $row['thumb']; ?>" alt="<?php echo $row['name']; ?>" class="img-responsive" style="width:65%;margin:0 auto;">
						</figure>
						<div class="buttonzone text-center">
							<a href="<?php echo $row['demo']; ?>" class="btn btn-success btn-border" target="_blank">XEM DEMO</a><a href="index.php?page=contact" class="btn btn-danger btn-border">ORDER CODE</a><a href="index.php?page=edit&id=<?php echo $row['id']; ?>" class="btn btn-warning btn-border">SỬA</a>
						</div>
					</div>
					
					<div class="col-md-12 col-md-offset-0 animate-box" data-animate-effect="fadeInLeft">
						<div class="col-md-12 contentthread">
							<p><?php echo $row['des']; ?></p>
                            <p class="titlesomepicture">Một số hình ảnh sản phẩm</p>
                        <div class="item">            
                            <div class="clearfix" style="">
                                <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                                    <?php
                                        $json = $row['listpic'];
                                        $ob = json_decode($json,false);
                                        foreach($ob as $mot){
                                    ?>
                                    <li data-thumb="images/<?php echo $mot; ?>"> 
                                        <img src="images/<?php echo $mot; ?>" style="width:100%" />
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
					</div>

				</div>
			</div>


			</div>
		</div>
	</div>
<?php }}?>
