<?php
ob_start();
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once('../config.php');
include ROOT."/inc/functions.php";
spl_autoload_register("loadClass");
$obj= new Db();
if ( isset($_POST['submit'])){
    $x = round(microtime(true) * 1000);
    $_SESSION['flash'] = "";
    $listpic = [];
    $thumb = "";
    for($i=0; $i<count($_FILES['file']['name']); $i++){
        $target_path = ROOT."/images/";
        $ext = explode('.', basename( $_FILES['file']['name'][$i]));
        $target_path = $target_path .$x."-".$_FILES['file']['name'][$i]; 
        if(move_uploaded_file($_FILES['file']['tmp_name'][$i], $target_path)) {
            array_push($listpic,$x."-".$_FILES['file']['name'][$i]);
        } else{
            $_SESSION['flash'] = "<div class='alert alert-danger text-center'>Lỗi khi Upload Hình ảnh sản phẩm !</div>";
            header("Location:../index.php?page=upload");
            exit;
        }   
    }
    if(isset($_FILES['thumb'])){
        $target_path = ROOT."/images/".$x."-".$_FILES['thumb']['name'];
        if(move_uploaded_file($_FILES['thumb']['tmp_name'], $target_path)) {
            $thumb = $x."-".$_FILES['thumb']['name'];
        } else{
            $_SESSION['flash'] = "<div class='alert alert-danger text-center'>Lỗi khi Upload Hình ảnh đại diện !</div>";
            header("Location:../index.php?page=upload");
            exit;
        } 
    }
    $tensp = $_POST['tensp'];
    $mota = $_POST['mota'];
    $key = $_POST['key'];
    $demo = $_POST['demo'];
    $listpic = json_encode($listpic);
    $sql = "INSERT INTO `products`(`name`, `des`, `thumb`, `listpic`,`demo`) 
                VALUES ('$tensp',:mota,'$thumb','$listpic','$demo')";
    $array[':mota'] = $mota;
    if($key == '0909274128'){
        $obj->select($sql, $array);
        $_SESSION['flash'] = "<div class='alert alert-success text-center'>Đã thêm sản phẩm</div>";
        header("Location:../index.php?page=upload");
        exit;
    }
    else{
        $_SESSION['flash'] = "<div class='alert alert-danger text-center'>Lỗi khi thêm sản phẩm</div>";
        header("Location:../index.php?page=upload");
        exit;
    }
}
?> 