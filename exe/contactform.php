<?php
    ob_start();
    session_start();
    require_once('../config.php');
    include ROOT."/inc/functions.php";
    spl_autoload_register("loadClass");
    $obj= new Db();
    if(isset($_POST['submit'])){
        $name = $_POST['name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $message = $_POST['message'];
        $sql = "INSERT INTO `contact`(`name`, `email`, `phone`, `message`) 
                VALUES (:ten,:email,:phone,:noidung)";
        $arr[":ten"] = $name;
        $arr[":email"] = $email;
        $arr[":phone"]  = $phone;
        $arr[":noidung"] = $message;
        $row = $obj->select($sql,$arr);
        if(isset($row)){
            $_SESSION['flash'] = "<div class='alert alert-success text-center'>Liên hệ của bạn đã được gửi đi, tôi sẽ sớm liên hệ lại với bạn.</div>";
            header("Location:../index.php?page=contact");
            
        }else{
            $_SESSION['flash'] = "<div class='alert alert-danger text-center'>Đã xảy ra lỗi, vui lòng thử lại</div>";
            header("Location:../index.php?page=contact");
        }
    }
?>