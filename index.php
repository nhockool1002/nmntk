
<?php
    ob_start();
    session_start();
    require_once('config.php');
    include ROOT."/inc/functions.php";
    spl_autoload_register("loadClass");
    $db= new Db();
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
    <?php require_once ROOT.'/templates/header.php'; ?>
	</head>
	<body>
	<div id="fh5co-page">
        <?php require_once ROOT.'/templates/leftmenu.php'; ?>
        <?php
        if(!isset($_GET["page"])){
            require_once ROOT.'/templates/products.php';
        }else{
              switch ($_GET["page"]) {
                case "view":
                    require_once ROOT.'/templates/details.php';
                    break;
                case "viewtopic":
                    require_once ROOT.'/templates/details-topic.php';
                    break;   
                case "upload":
                    require_once ROOT.'/templates/upload.php';
                    break;
                case "upload-topic":
                    require_once ROOT.'/templates/upload-topic.php';
                    break;
                case "about":
                    require_once ROOT.'/templates/pdf.php';
                    break;
                case "contact":
                    require_once ROOT.'/templates/contact.php';
                    break; 
                case "edit":
                    require_once ROOT.'/templates/edit.php';
                    break;
                case "edittopic":
                    require_once ROOT.'/templates/edittopic.php';
                    break;
                case "topic":
                    require_once ROOT.'/templates/topic.php';
                    break;   
                case "login":
                    require_once ROOT.'/templates/login.php';
                    break;
                case "logout":
                    require_once ROOT.'/templates/logout.php';
                    break;           
                default:
                require_once ROOT.'/templates/products.php';
            }
        }
        ?>
		</div>
	</div>
    <?php require_once ROOT.'/templates/footer.php'; ?>
	</body>
</html>

